# BASH start tooltip

## Purpose

This short BASH script will determine which kind of user is logging into a
terminal window, ROOT or normal user, and display a banner accordingly.

If the user is logging in as ROOT, a giant "ROOT" banner will display along with
a warning to be careful.

If the user is logging in as a normal user, and into a TTY command terminal, a
cowsay banner will be displayed with a fortune and a quick terminal tip.

If the user is logging in as a normal user, and into a terminal emulator, a
small banner will display, as to not take up retail space in new terminal
windows.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_start_tooltip.git

## Placement

This script is best placed within your `bashrc` (for Linux),
`/etc/bash.bashrc` (for Linux, globally), or `.bash_profile` script, or sourced
within one of those files.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_start_tooltip/src/48d3e2b5959ebdbd9ebe2234acc6476c591191bc/LICENSE.txt?at=master) file for
details.

