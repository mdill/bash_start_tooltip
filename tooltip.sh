#!/bin/bash

# Clear the screen, securely
printf "\033c";

if [[ $EUID == 0 ]]; then
    printf "\e[1;31m  ______  _____   _____  _______\n |_____/ |     | |     |    |\n |    \_ |_____| |_____|    |\n\n\e[0m With great power comes great responsibility\n\n";
elif [[ $0 == "-bash" ]]; then
    fortune -s | cowsay -d;
    echo "Did you know that: "; whatis $(ls /bin | shuf -n 1);
else
    printf "I'm going to get SOMETHING done...\n\n";
fi

